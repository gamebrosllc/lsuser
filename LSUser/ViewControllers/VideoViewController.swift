//
//  VideoViewController.swift
//  LSUser
//
//  Created by GameBros on 3/27/19.
//  Copyright © 2019 Gnerre. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit


class VideoViewController: UIViewController, AVPlayerViewControllerDelegate, VideoSelectDelegate {
    @IBOutlet weak var videoThumbnailView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
//    var videoName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playButton.layer.cornerRadius = 12
        playButton.layer.masksToBounds = true
        
        print("viewDidLoad")
        print(videoThumbnailView)

//        videoName = "Sarah Christianson"
//        setupVideo()

        let pathToEx1 = Bundle.main.path(forResource: VideoName.selected.rawValue, ofType: "mp4")
        let pathURL = NSURL.fileURL(withPath: pathToEx1!)
        let asset = AVAsset(url: pathURL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTimeMake(value: 1, timescale: 1)
        let imageRef = try! imageGenerator.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage:imageRef)
        videoThumbnailView.image = thumbnail
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
    }
    
    func setupVideo() {
        print("trying to setup")
        if playerLayer != nil {
            playerLayer.removeFromSuperlayer()
        }
        
        let pathToEx1 = Bundle.main.path(forResource: VideoName.selected.rawValue, ofType: "mp4")
        let pathURL = NSURL.fileURL(withPath: pathToEx1!)
        
        player = AVPlayer(url: pathURL)
        
        let asset = AVAsset(url: pathURL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTimeMake(value: 1, timescale: 1)
        let imageRef = try! imageGenerator.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage:imageRef)
        videoThumbnailView.image = thumbnail
        
        playButton.isHidden = false

    }
    
    func playVideo() {
//        let pathToEx1 = Bundle.main.path(forResource: VideoName.selected.rawValue, ofType: "mp4")
//        let pathURL = NSURL.fileURL(withPath: pathToEx1!)
//
//        player = AVPlayer(url: pathURL)

        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
    
        player.play()
    }
    
    func pauseVideo() {
        if player != nil {
            player.pause()
        }
    }
    
    @IBAction func tapPressed(_ sender: Any) {
        if player == nil { return }
        
        if player.isPlaying {
            player.pause()
        } else {
            player.play()
        }
        
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        playVideo()
        playButton.isHidden = true
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
