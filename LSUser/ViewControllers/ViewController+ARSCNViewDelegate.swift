/*
See LICENSE folder for this sample’s licensing information.

Abstract:
ARSCNViewDelegate interactions for `ViewController`.
*/

import ARKit

extension ViewController: ARSCNViewDelegate, ARSessionDelegate {
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        let isAnyObjectInView = virtualObjectLoader.loadedObjects.contains { object in
            return sceneView.isNode(object, insideFrustumOf: sceneView.pointOfView!)
        }
        
        DispatchQueue.main.async {
            self.virtualObjectInteraction.updateObjectToCurrentTrackingPosition()
//            self.updateFocusSquare(isObjectVisible: isAnyObjectInView)
        }
        
        
        // If light estimation is enabled, update the intensity of the directional lights
        if let lightEstimate = session.currentFrame?.lightEstimate {
            sceneView.updateDirectionalLighting(intensity: lightEstimate.ambientIntensity, queue: updateQueue)
        } else {
            sceneView.updateDirectionalLighting(intensity: 1000, queue: updateQueue)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        print("node added, anchor name: ", anchor.name)
//        switch anchor.name {
//        case "Room Scale Rig":
//            for object in virtualObjectLoader.loadedObjects {
//                if object.modelName == anchor.name {
////                        self.sceneView.prepare([object], completionHandler: { _ in
//                            DispatchQueue.main.async {
//                                object.reset()
//                                node.addChildNode(object)
//                                object.isHidden = false
//                            }
////                        })
//                }
//            }
//        case "Full Size Rig":
//            for object in virtualObjectLoader.loadedObjects {
//                if object.modelName == anchor.name {
//
////                        self.sceneView.prepare([object], completionHandler: { _ in
//                            DispatchQueue.main.async {
//                                node.addChildNode(object)
//
//                                object.isHidden = false
//                            }
////                        })
//                }
//            }
//        case "Photos":
//            for object in virtualObjectLoader.loadedObjects {
//                if object.modelName == anchor.name {
//
////                        self.sceneView.prepare([object], completionHandler: { _ in
//                            DispatchQueue.main.async {
//                                object.reset()
//
//                                node.addChildNode(object)
//
//                                object.isHidden = false
//                            }
////                        })
//                }
//            }
//        case "Photos_FrackingDonkey":
//            for object in virtualObjectLoader.loadedObjects {
//                if object.modelName == anchor.name {
//
////                        self.sceneView.prepare([object], completionHandler: { _ in
//                            DispatchQueue.main.async {
//                                node.addChildNode(object)
//
//                                object.isHidden = false
//                            }
////                        })
//                }
//            }
//        default:
//            break
//        }
        
        switch anchor.name {
        case "Room Scale Rig":
            for object in VirtualObject.availableObjects {
                if object.modelName == anchor.name {
                    virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in

                        self.sceneView.prepare([object], completionHandler: { _ in
                            DispatchQueue.main.async {
                                node.addChildNode(object)

                                loadedObject.isHidden = false
                            }
                        })
                    })
                }
            }
        case "Full Size Rig":
            for object in VirtualObject.availableObjects {
                if object.modelName == anchor.name {
                    virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in

                        self.sceneView.prepare([object], completionHandler: { _ in
                            DispatchQueue.main.async {
                                node.addChildNode(object)

                                loadedObject.isHidden = false
                            }
                        })
                    })
                }
            }
        case "Photos":
            for object in VirtualObject.availableObjects {
                if object.modelName == anchor.name {
                    virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in

                        self.sceneView.prepare([object], completionHandler: { _ in
                            DispatchQueue.main.async {
                                node.addChildNode(object)

                                loadedObject.isHidden = false
                            }
                        })
                    })
                }
            }
        case "Photos_FrackingDonkey":
            for object in VirtualObject.availableObjects {
                if object.modelName == anchor.name {
                    virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in

                        self.sceneView.prepare([object], completionHandler: { _ in
                            DispatchQueue.main.async {
                                node.addChildNode(object)

                                loadedObject.isHidden = false
                            }
                        })
                    })
                }
            }
        case "Photos_608":
            for object in VirtualObject.availableObjects {
                if object.modelName == anchor.name {
                    virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in
                        
                        self.sceneView.prepare([object], completionHandler: { _ in
                            DispatchQueue.main.async {
                                node.addChildNode(object)
                                
                                loadedObject.isHidden = false
                            }
                        })
                    })
                }
            }
        default:
            break
        }
        
//        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//
//        updateQueue.async {
//            for object in self.virtualObjectLoader.loadedObjects {
//                object.adjustOntoPlaneAnchor(planeAnchor, using: node)
//            }
//        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
//        updateQueue.async {
//            if let planeAnchor = anchor as? ARPlaneAnchor {
//                for object in self.virtualObjectLoader.loadedObjects {
//                    object.adjustOntoPlaneAnchor(planeAnchor, using: node)
//                }
//            } else {
//                if let objectAtAnchor = self.virtualObjectLoader.loadedObjects.first(where: { $0.anchor == anchor }) {
//                    objectAtAnchor.simdPosition = anchor.transform.translation
//                    objectAtAnchor.anchor = anchor
//                }
//            }
//        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        var message: String = ""
        
        switch camera.trackingState {
        case .notAvailable, .limited:
            message = camera.trackingState.localizedFeedback
        case .normal:
            message = ""
             self.snapshotThumbnail.isHidden = true
            // Unhide content after successful relocalization.
            virtualObjectLoader.loadedObjects.forEach { $0.isHidden = false }
        }
        
        DispatchQueue.main.async {
            self.messageLabel.text = message
            self.messageBlur.isHidden = message.isEmpty
        }
  
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard error is ARError else { return }

        let errorWithInfo = error as NSError
        let messages = [
            errorWithInfo.localizedDescription,
            errorWithInfo.localizedFailureReason,
            errorWithInfo.localizedRecoverySuggestion
        ]

        let errorMessage = messages.compactMap({ $0 }).joined(separator: "\n")

        DispatchQueue.main.async {
            self.messageLabel.text = "The AR session failed.\n\(errorMessage)"
            self.messageBlur.isHidden = messages.isEmpty
        }
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Hide content before going into the background.
        virtualObjectLoader.loadedObjects.forEach { $0.isHidden = true }
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        /*
         Allow the session to attempt to resume after an interruption.
         This process may not succeed, so the app must be prepared
         to reset the session if the relocalizing status continues
         for a long time -- see `escalateFeedback` in `StatusViewController`.
         */
        return true
    }
}
