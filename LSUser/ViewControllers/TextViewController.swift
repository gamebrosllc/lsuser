//
//  TextViewController.swift
//  LSUser
//
//  Created by Gnerre on 4/9/19.
//  Copyright © 2019 Gnerre. All rights reserved.
//

import UIKit

class TextViewController: UIViewController, TextDelegate {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var citationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setupText(textSelect: TextSelection, citation: Citation) {
        textView.text = textSelect.rawValue
        citationLabel.text = citation.rawValue
    }
}


