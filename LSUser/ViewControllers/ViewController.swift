//
//  ViewController.swift
//  LSUser
//
//  Created by Gnerre on 3/19/19.
//  Copyright © 2019 Gnerre. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CloudKit

protocol VideoSelectDelegate {
    func setupVideo()
    func pauseVideo()
}

protocol TextDelegate {
    func setupText(textSelect: TextSelection, citation: Citation)
}

class ViewController: UIViewController, NodeTapDelegate {

    @IBOutlet var sceneView: VirtualObjectARView!
    @IBOutlet weak var snapshotThumbnail: UIImageView!
    @IBOutlet weak var messageBlur: UIVisualEffectView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var closePopupButton: UIButton!
    @IBOutlet weak var toggleAudioButton: UIButton!
    
    var delegate: VideoSelectDelegate?
    var containerViewController: VideoViewController?
    
    @IBOutlet weak var textContainerView: UIView!
    var textDelegate: TextDelegate?
    var textContainerViewController: TextViewController?
    
    /// Coordinates the loading and unloading of reference nodes for virtual objects.
    let virtualObjectLoader = VirtualObjectLoader()
    /// A serial queue used to coordinate adding or removing nodes from the scene.
    let updateQueue = DispatchQueue(label: "co.gamebros.lsuser.serialSceneKitQueue")
    lazy var virtualObjectInteraction = VirtualObjectInteraction(sceneView: sceneView)

    var screenCenter: CGPoint {
        let bounds = sceneView.bounds
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    /// Convenience accessor for the session owned by ARSCNView.
    var session: ARSession {
        return sceneView.session
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        sceneView.session.delegate = self
        
        virtualObjectInteraction.delegate = self
        
        self.snapshotThumbnail.isHidden = true
        setupCamera()        
        sceneView.setupDirectionalLighting(queue: updateQueue)
        
        messageBlur.layer.cornerRadius = 8
        messageBlur.layer.masksToBounds = true
        
        restartButton.layer.cornerRadius = 8
        restartButton.layer.masksToBounds = true

        textContainerView.layer.cornerRadius = 8
        textContainerView.layer.masksToBounds = true
        textContainerView.isHidden = true
        
        videoContainerView.isHidden = true
        
        closePopupButton.layer.cornerRadius = 8
        closePopupButton.layer.masksToBounds = true
        closePopupButton.isHidden = true
        
        toggleAudioButton.layer.cornerRadius = 8
        toggleAudioButton.layer.masksToBounds = true
        toggleAudioButton.isHidden = true
        
//        virtualObjectLoader.loadAllObjects(scene: sceneView)
        
        loadExperience()
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        // make sure that the value is an optional so that you can unset it later. This one is optional because we use optional casting (with as?)
//        let vc = storyboard.instantiateViewController(withIdentifier: "vvController") as? VideoViewController
//        self.delegate = vc
        
//        for object in VirtualObject.availableObjects {
//            if object.modelName == anchor.name {
//        virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in
//            self.sceneView.prepare([object], completionHandler: { _ in
//                DispatchQueue.main.async {
//                    self.placeVirtualObject(loadedObject)
//                    loadedObject.isHidden = false
//                }
//            })
//        })
//            }
//        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func setupCamera() {
        guard let camera = sceneView.pointOfView?.camera else {
            fatalError("Expected a valid `pointOfView` from the scene.")
        }
        
        /*
         Enable HDR camera settings for the most realistic appearance
         with environmental lighting and physically based materials.
         */
        camera.wantsHDR = true
        camera.exposureOffset = -1
        camera.minimumExposure = -1
        camera.maximumExposure = 3
    }
    
    let photoList = [
        "DSC00409",
        "DSC00625",
        "DSC00660",
        "DSC00698",
        "DSC00608"
    ]
    
    func handleTap(results: [SCNHitTestResult]) {
        textContainerView.isHidden = true
        videoContainerView.isHidden = true
        closePopupButton.isHidden = true
        delegate?.pauseVideo()
        
        print("tap called")
        let last = results.last?.node
        let oneUp = last?.parent
        let twoUp = oneUp?.parent
        
//        print(last, oneUp, twoUp)
        
        if twoUp?.name == "sarahTarget" {
            textDelegate?.setupText(textSelect: TextSelection.Sarah, citation: .none)
            textContainerView.isHidden = false
            VideoName.selected = .Sarah
            videoContainerView.isHidden = false
            closePopupButton.isHidden = false
            self.delegate?.setupVideo()

        } else if twoUp?.name == "terryTarget" {
            textDelegate?.setupText(textSelect: TextSelection.Terry, citation: .none)
            textContainerView.isHidden = false
            VideoName.selected = .Terry
            self.delegate?.setupVideo()
            videoContainerView.isHidden = false
            closePopupButton.isHidden = false

//            self.delegate?.setupVideo()
        } else if twoUp?.name == "LargeTargetOne" {
            textDelegate?.setupText(textSelect: TextSelection.Fracking, citation: .Fracking)
            textContainerView.isHidden = false
            closePopupButton.isHidden = false

            toggleAudioButton.isHidden = false
        }
        
        if let oneUpName = oneUp?.name {
            if photoList.contains(oneUpName) {
                if let captionNode = oneUp?.childNode(withName: "caption", recursively: true) {
                    if captionNode.isHidden {
                        captionNode.isHidden = false
                    } else {
                        captionNode.isHidden = true
                    }
                }
            }
        }
        
//        if oneUp?.name == "DSC00409" {
//            if let textNode = oneUp?.childNode(withName: "DSC00409mT", recursively: true) {
//
//
//            if textNode.isHidden {
//                textNode.isHidden = false
//            } else {
//                textNode.isHidden = true
//            }
//        }
//        }
        
//        if last?.name == "DSC00625" {
//            textDelegate?.setupText(textSelect: TextSelection.DSC00625, citation: .DSC00625)
//            textContainerView.isHidden = false
//            closePopupButton.isHidden = false
//
//        } else if last?.name == "DSC00660" {
//            textDelegate?.setupText(textSelect: TextSelection.DSC00660, citation: .DSC00660)
//            textContainerView.isHidden = false
//            closePopupButton.isHidden = false
//
//        } else if last?.name == "DSC00698" {
//            textDelegate?.setupText(textSelect: TextSelection.DSC00698, citation: .DSC00698)
//            textContainerView.isHidden = false
//            closePopupButton.isHidden = false
//
//        } 
        
//        switch GameState.current {
//        case .starting:
//            if results.last?.node.name == "box" {
//                let root = self.sceneView.scene.rootNode
//                root.childNode(withName: "targetScene", recursively: false)?.removeFromParentNode()
//
//                self.roomTarget?.isHidden = false
//
//                notificationLabel.text = "Tap the Place Model button to secure the rig to the floor"
//
//                GameState.current.nextLevel()
//            }
//        case .firstStory:
//            if results.last?.node.name == "box" {
//                guard !terryActive else { return }
//                self.selectedAudio = .terry
//                self.terryActive = true
//                self.sarahActive = false
//                self.elizabethActive = false
//                self.storyDidChange = true
//                self.videoName = .terry
//                self.showStory()
//            }
//        default:
//            if twoUp?.name == "smallTarget" {
//                //terry
//                //                guard !terryActive else { return }
//                self.selectedAudio = .terry
//                self.terryActive = true
//                self.sarahActive = false
//                self.elizabethActive = false
//                self.storyDidChange = true
//                self.videoName = .terry
//                self.showStory()
//            } else if twoUp?.name == "largeTarget" {
//                //sarah
//                print("touched")
//                //                guard !sarahActive else { return }
//                self.selectedAudio = .sarah
//                self.terryActive = false
//                self.sarahActive = true
//                self.elizabethActive = false
//                self.storyDidChange = true
//                self.videoName = .sarah
//                self.showStory()
//            } else if results.last?.node.name == "hiddenTargetElizabeth" {
//                guard !elizabethActive else { return }
//                self.selectedAudio = .elizabeth
//                self.terryActive = false
//                self.sarahActive = false
//                self.elizabethActive = true
//                self.storyDidChange = true
//                self.showStory()
//            }
//        }
        
        
    }
    
//    func placeVirtualObject(_ virtualObject: VirtualObject) {
//        virtualObjectInteraction.translate(virtualObject, basedOn: screenCenter, infinitePlane: false, allowAnimation: false)
//        virtualObjectInteraction.selectedObject = virtualObject
//
//        updateQueue.async {
//            //            self.sceneView.scene.rootNode.addChildNode(virtualObject)
//            self.sceneView.addOrUpdateAnchor(for: virtualObject)
//        }
//    }


    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
 
    
    func loadExperience() {
        CloudHelper.shared.getMapFromCloud {
            var mapDataFromFile: Data? {
                let asset = CloudHelper.currentLocation["mapDataAsset"] as? CKAsset
                return try? Data(contentsOf: asset!.fileURL)
            }
            
            
//            var mapData: Data?
//
//            if let mapAsset = CloudHelper.currentLocation["mapDataAsset"] as? CKAsset {
//                mapData = Data(contentsOf: mapAsset.fileURL)
//            }
            /// - Tag: ReadWorldMap
            let worldMap: ARWorldMap = {
                guard let data = mapDataFromFile
                    else { fatalError("Map data should already be verified to exist before Load button is enabled.") }
                do {
                    guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data as! Data)
                        else { fatalError("No ARWorldMap in archive.") }
                    return worldMap
                } catch {
                    fatalError("Can't unarchive ARWorldMap from file data: \(error)")
                }
            }()
            
            // Display the snapshot image stored in the world map to aid user in relocalizing.
            if let snapshotData = CloudHelper.currentLocation["snapshotData"],
                let snapshot = UIImage(data: snapshotData as! Data) {
                DispatchQueue.main.async {
                    self.snapshotThumbnail.image = snapshot
                    self.snapshotThumbnail.isHidden = false
                }
            } else {
                print("No snapshot image in world map")
            }
            // Remove the snapshot anchor from the world map since we do not need it in the scene.
            worldMap.anchors.removeAll(where: { $0 is SnapshotAnchor })
            
            let configuration = self.defaultConfiguration // this app's standard world tracking settings
            configuration.initialWorldMap = worldMap
            self.sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
            
            //        isRelocalizingMap = true
            //        virtualObjectAnchor = nil
            
        }
    }
    
    var defaultConfiguration: ARWorldTrackingConfiguration {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        configuration.environmentTexturing = .automatic
        return configuration
    }
    
    @IBAction func closePopupButtonPressed(_ sender: Any) {
        videoContainerView.isHidden = true
        textContainerView.isHidden = true
        closePopupButton.isHidden = true
    }
    
    @IBAction func toggleAudioButtonPressed(_ sender: Any) {
        if let rig = sceneView.scene.rootNode.childNode(withName: "LargeTargetOne", recursively: true) {
        
        if toggleAudioButton.isSelected {
            rig.removeSound()
            toggleAudioButton.isSelected = false
        } else {
            rig.attachSound(sound: .frackingSound)
            toggleAudioButton.isSelected = true
        }
        }
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Restart?", message: "Do you want to restart the entire experience?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
            self.loadExperience()
        }))
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: {_ in
            return
        }))
    
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedSegue" {
            containerViewController = segue.destination as? VideoViewController
            delegate = containerViewController
        } else if segue.identifier == "textEmbedSegue" {
            textContainerViewController = segue.destination as? TextViewController
            textDelegate = textContainerViewController
        }
    }
}
