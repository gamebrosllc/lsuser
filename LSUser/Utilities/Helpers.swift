//
//  Helpers.swift
//  LSUser
//
//  Created by GameBros on 4/7/19.
//  Copyright © 2019 Gnerre. All rights reserved.
//
import SceneKit
import Foundation

enum VideoName: String {
    case Sarah = "Sarah Christianson"
    case Terry = "Terry Evans"
    
    static var selected: VideoName = .Sarah
}

enum TextSelection: String {
    case DSC00625 = "Companies begin the drilling process on a parcel of land of approximately 3 acres. This may be slightly smaller or larger, depending upon the topography of the land. The land should be large enough to accommodate the trucks and equipment needed for the operation."
    
    case DSC00660 = "The terms 'hydraulic fracturing' and 'fracking' are sometimes used in different ways. Some use the terms to refer to only one part of the oil and gas extraction process, where shale is fractured far below the surface to provide access to oil and natural gas trapped in the rock. Others use the term to refer to the entire life-cycle of the well, including post-drilling operation and maintaining and operating surface facilities like compressors, storage ponds, and pipelines."
    
    case DSC00698 = "Flares can release pollutants, depending upon the chemical composition of the gas being burned. According to the Ventura County Air Pollution Control District (California), potential pollutants from natural gas flares include benzene, formaldehyde, polycyclic aromatic hydrocarbons (PAHs, including naphthalene), acetaldehyde, acrolein, propylene, toluene, xylenes, ethyl benzene and hexane. North Dakota oil companies have flared more than $854 million of natural gas since 2010."
    
    case DSC00409 = "It takes one week to ten days to drill a single well. On a site that uses hydraulic fracturing, multiple wells can be drilled on a a small surface footprint using a technique known as horizontal or directional drilling. In it, the pipes turn hoizontally to tap into other well sites from the same surface location. The large containers on a well site, as seen in this photograph, are used to store oil and/or natural gas; the 'donkey' conveyance helps bring oil or natural gas to the surface."
    
    case Sarah = "Sarah Christianson photographs the land surrounding her 4th generation family farm north of Fargo, ND, and her family's homestead land in Western North Dakota.\n\nShe grew up in North Dakota, and currently lives in San Francisco.\n\nWatch her video below."
    
    case Terry = "Chicago photographer, Terry Evans, has photographed the prairies and plains for forty years.\n\nShe is searching to photograph how people and land use affect each other.\n\nWatch her video below."
    
    case Fracking = "Hydraulic Fracturing (or fracking) uses high-pressure liquids to release oil and natural gas from underground rocks. A platform like this is the first step in the process to create a well by fracking.\n\nIt is used to drill wells up to 10,000 feet deep. The well channels are sealed with steel casings to protect groundwater and help stop gas leaks. 3 to 5 million gallons of water, combined with sand and chemicals, are pumped into the wells to create fractures in the rock and release the oil or gas, which then flows to the surface.\n\nPress the button in the top left to hear audio recorded at a fracking site."
}

enum Citation: String {
    case none = ""
    
    case DSC00625 = "Source: Greeley Tribune (https://www.greeleytribune.com/news/fracking-101-breaking-down-the-most-important-part-of-todays-oil-gas-drilling/)"
    
    case DSC00660 = "Source: Center of the American West (https://www.centerwest.org/projects/energy/glossary#h)"
    
    case DSC00698 = "Source: Earthworks (https://earthworks.org/issues/flaring_and_venting/)"
    
    case DSC00409 = "Source: Center of the American West https://www.centerwest.org/projects/energy/glossary#h"
    
    case Fracking = "Sources: Columbia University Earth Institute (http://www.earth.columbia.edu/articles/view/1791), U.S. News and World Report (https://www.usnews.com/topics/subjects/fracking)"

}

enum Sounds: String {
    case frackingSound = "Fracking_Sounds.mp3"
    case terry = "Terry_Evans_1"
    case sarah = "Sarah_Christianson_1"
    case elizabeth = "Elizabeth_Farnsworth_2"
}

struct AudioHelper {
    static let availableSounds:[Sounds: SCNAudioSource] = {
        let frackingSource = SCNAudioSource(fileNamed: "art.scnassets/Sounds/\(Sounds.frackingSound.rawValue)")
        frackingSource?.loops = true
        frackingSource?.load()
        
        return [
            .frackingSound:frackingSource!
        ]
    }()
}

extension SCNNode {
    func attachSound(sound :Sounds) {
        self.addAudioPlayer(SCNAudioPlayer(source: AudioHelper.availableSounds[sound]!))
    }
    
    func removeSound() {
        self.removeAllAudioPlayers()
    }
}
