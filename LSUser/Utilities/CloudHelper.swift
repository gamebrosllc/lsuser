//
//  CloudHelper.swift
//  ARPersistence
//
//  Created by Gnerre on 10/17/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import CloudKit
import ARKit

class SavedLocation {
//    static var selectedLocation: SavedLocation?
//    static var shared = SavedLocation()
    
//    var mapData: Data!
    var record: CKRecord!
    
//    func convertRecord(record: CKRecord) -> SavedLocation {
//        let gp = SavedLocation()
//        gp.record = record
////        gp.mapData = record["mapData"]
//
//        return gp
//    }
}

class CloudHelper {
    static let shared = CloudHelper()
    static var currentLocation = CKRecord(recordType: "SavedLocation")
    
    private let recordID = CKRecord.ID.init(recordName: "map")
    
    let container = CKContainer(identifier: "iCloud.co.gamebros.LSADmin")
    
    let locationManager = CLLocationManager()
    
    func getMapFromCloud(onCompletion: @escaping () -> (Void)) {
        let publicDatabase = container.publicCloudDatabase
        publicDatabase.fetch(withRecordID: recordID) { (record, error) in
            if error != nil { print(error?.localizedDescription)
                return
            }
            
            CloudHelper.currentLocation = record!
            onCompletion()
        }

    }
    
//    func saveWorldMap(record: CKRecord) {
//        let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: [])
//        saveOperation.perRecordCompletionBlock = { (record: CKRecord?, error: Error?) -> Void in
//            if let error = error {
//                print(error.localizedDescription)
//                return
//            }
//            print("map saved!")
//        }
//
//        publicDatabase.add(saveOperation)
//
////            publicDatabase.save(mapRecord) {
////                (record, error) in
////                if let error = error {
////                    print(error.localizedDescription)
////                    return
////                }
////                print("map saved!")
////            }
//    }
    
}
